# Matemáticas 
## Conjuntos, Aplicaciones y funciones
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .yellow {
    BackgroundColor yellow
  }
  .orange {
    BackgroundColor orange
  }
  .cyan {
    BackgroundColor cyan
  }
  .lime {
    BackgroundColor lime
  }
  .coral {
    BackgroundColor coral
  }
  .red {
    BackgroundColor red
  }
  .gold {
    BackgroundColor gold
  }
  .silver {
    BackgroundColor silver
  }
  .pink {
    BackgroundColor pink
  }
  .purple {
    BackgroundColor purple
  }
}
</style>
* Conjuntos, Aplicaciones y funciones
** Operaciones <<green>>
***_ con
**** Insercción <<green>>
*****_ forma
****** Conjuntos <<green>>
*******_ con
******** Elementos comúnes <<green>>
*********_ involucrados en la
********** Operación <<green>>
**** Complemento <<green>>
*****_ es un
****** Conjunto <<green>>
*******_ que no
******** Pertenece <<green>>
*********_ a ningún 
********** Otro conjunto dado <<green>>
**** Unión <<green>>
*****_ nos permite
****** Unir <<green>>
*******_ dos o mas
******** Conjuntos <<green>>
*********_ para formar un único
********** Conjunto <<green>>
***********_ de
************ Elementos <<green>>
*************_ sin
************** Repetir <<green>>
** Representación <<rose>>
***_ se usa el
**** Diagrama de Venn <<rose>>
*****_ llamado asi por
****** John Venn <<rose>>
*******_ un
******** Lógico británico <<rose>>
*****_ consistiendo en
****** Circulos <<rose>>
*******_ usados para
******** Representar <<rose>>
*********_ las
********** Relaciones Lógicas <<rose>>
***********_ entre
************ Varios conjuntos <<rose>>
*****_ usados en
****** Áreas <<rose>>
*******_ como son
******** Matemática <<rose>>
******** Lógica <<rose>>
******** Estadística <<rose>>
******** Informática <<rose>>
** Tipos de Conjuntos <<blue>>
*** Finito <<blue>>
****_ aquellos
***** Conjuntos <<blue>>
******_ con
******* Cardinalidad definida <<blue>>
*** Infinito <<blue>>
****_ aquel donde la
***** Cardinalidad <<blue>>
******_ no está
******* Definida <<blue>>
********_ por ser muy
********* Grande <<blue>>
**********_ como para ser
*********** Cuantificado <<blue>>
*** Vacío <<blue>>
****_ aquel que
***** Carece <<blue>>
******_ de
******* Elementos <<blue>>
@endmindmap
```
## Funciones
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .yellow {
    BackgroundColor yellow
  }
  .orange {
    BackgroundColor orange
  }
  .cyan {
    BackgroundColor cyan
  }
  .lime {
    BackgroundColor lime
  }
  .coral {
    BackgroundColor coral
  }
  .red {
    BackgroundColor red
  }
  .gold {
    BackgroundColor gold
  }
  .silver {
    BackgroundColor silver
  }
  .pink {
    BackgroundColor pink
  }
  .purple {
    BackgroundColor purple
  }
}
</style>
* Funciones
** Objetivo
***_ son el
**** Nucleo
*****_ de las
****** Matemáticas
*******_ manejando
******** Conjuntos
*********_ de 
********** Números Reales
** Limites
***_ son
**** Clasificadas
*****_ por
****** Continuas
*******_ cuyo
******** Gráfico
*********_ puede
********** Dibujarse
***********_ con un 
************ Trazo
****** Discontinuas
*******_ presenta uno o varios
******** Puntos
*********_ donde existen
********** Saltos
***********_ y el
************ Grafico
*************_ se
************** Rompe
** Caracteristicas
***_ la
**** Función
*****_ es una
****** Relacion
*******_ entre dos
******** Conjuntos
***_ para cada
**** Variable x
*****_ en el 
****** Dominio
*******_ existe un
******** Valor único
***_ constan de
**** Valores crecientes
**** Valores decrecientes
**** Máximos y minimos
**** Rango
**** Dominio
**** Codominio
@endmindmap
```
## La matemática del computador
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .yellow {
    BackgroundColor yellow
  }
  .orange {
    BackgroundColor orange
  }
  .cyan {
    BackgroundColor cyan
  }
  .lime {
    BackgroundColor lime
  }
  .coral {
    BackgroundColor coral
  }
  .red {
    BackgroundColor red
  }
  .gold {
    BackgroundColor gold
  }
  .silver {
    BackgroundColor silver
  }
  .pink {
    BackgroundColor pink
  }
  .purple {
    BackgroundColor purple
  }
}
</style>
* La matemática del computador
** Sistema Binario
***_ que es donde se
**** Basa
****_ utiliza la
***** Aritmética Binaria
****_ es un
***** Sistema simple
******_ de dos
******* Posiciones
********_ uniendo la
********* Lógica booleana
**********_ con la
*********** Pertenencia
************_ y no
************* Pertenencia
**************_ de
*************** Conjuntos
********_ pasando por
********* Corriente 0
**********_ a
*********** Corriente 1
************_ y
************* Viceversa
** Aritmética finita
***_ tiene
**** Limitaciones 
*****_ en los
****** Medios fisicos
*******_ donde no puede
******** Representar
*********_ los 
********** Números
***********_ con
************ Infinitos décimales
**** Números finitos
*****_ de
****** Posiciones
*******_ que cambian
******** Valores de 0 y 1
*********_ en
********** Series de posición de memoria
** Codificación 
***_ saca
**** Representaciones
*****_ de
****** Simbolos
******* Letras
******* Números
***_ para poder
**** Representar
*****_ los
****** Números muy grandes
*******_ o
******** Números muy pequeños
*********_ se usa la
********** Notación científica
***********_ en forma del
************ Producto
*************_ de un
************** Número con potencia diez
***********_ se puede
************ Acortar
*************_ las
************** Cifras en binario
*************** Octal
*************** Decimal
@endmindmap
```
